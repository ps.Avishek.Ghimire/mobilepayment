import http from 'k6/http';
import { uuidv4 } from '../K6/core_function.js';
var count = 0;
var start_from = 1333;
var tenant = "KHAL";
export let options = {
 //// virtual user
 vus: 1,
 rps: '2',
 //duration: '60s',
 iterations: 2
};

export default function() {
 const CustReg = `{
 "operation":"cashin",
 "sender":"KHALCI",
 "senderType":"C",
 "deviceId":"3",
 "lang":1,
 "msgId":"CashIn-${count}${uuidv4()}",
 "tenant":"${tenant}",
 "extraData":[
 {
 "key":"amount",
 "value":"115"
 },
 {
 "key":"charges",
 "value":"0"
 },
 {
 "key":"receiver",
 "value":"009740000${start_from}"
 },
 {
 "key":"receiverType",
 "value":"M"
 },
 {
 "key":"bankAccount",
 "value":"1234567"
 }
 ]
 }`;
 start_from++;
 count++;
 var params = {
 headers: {
 'Content-Type': 'application/json',
 },
 };
 var Coreurl = 'http://192.168.1.249:8080/mpay/soap/process/operation';
 console.log(Coreurl);
 http.post(Coreurl, CustReg, params);
 count = count + 1;
}
