import http from 'k6/http';
import { uuidv4 } from '../K6/core_function.js';
var count = 0;
var start_from = ${__ENV.startfrom};
var tenant = 'KHAL';

export let options = {
    //// virtual user
    vus: 1,
    rps: '2',
    iterations: `${__ENV.no_of_iteration}`
    
    //duration: '60s',
    
};

export default function() {
  const CustReg = `{
    "deviceID":3,
    "lang":1,
    "msgId":"Cust-${count}-${uuidv4()}",
    "operation":"cust",
    "sender":"009740000${start_from}",
    "senderType":"M",
    "tenant":"${tenant}",
    "extraData":[
       {
          "key":"firstName",
          "value":"Clusus"
       },
       {
          "key":"midName",
          "value":"Test"
       },
       {
          "key":"lastName",
          "value":"Mass"
       },
       {
          "key":"idTypeCode",
          "value":"NIDN"
       },
       {
          "key":"idNumber",
          "value":"009740000${start_from}"
       },
       {
          "key":"dateOfBirth",
          "value":"31/12/1983"
       },
       {
          "key":"countryCode",
          "value":"QAT"
       },
       {
          "key":"cityCode",
          "value":"1"
       },
       {
          "key":"address",
          "value":"Amman Jordan - Mecca st"
       },
       {
          "key":"mobileNumber",
          "value":"009740000${start_from}"
       },
       {
          "key":"reference",
          "value":"009740000${start_from}"
       },
       {
          "key":"prefLanguage",
          "value":"1"
       },
       {
          "key":"phone1",
          "value":""
       },
       {
          "key":"email",
          "value":"m@m.com"
       },
       {
          "key":"profileCode",
          "value":"Default"
       },
       {
          "key":"gender",
          "value":"2"
       },
       {
          "key":"notes",
          "value":"Test customer"
       }
    ]
  }`

  start_from++;
  count++;
  var params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  var Coreurl = 'http://192.168.1.249:8080/mpay/soap/process/operation';
  console.log(Coreurl);
  http.post(Coreurl, CustReg, params);
  count = count + 1;
}
