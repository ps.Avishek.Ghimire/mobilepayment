import http from 'k6/http';
import { uuidv4 } from '../K6/core_function.js';
 
var count = 0;
var start_from = 1333;
var tenant = "KHAL";
 
 
export let options = {
 //// virtual user
 vus: 1,
 rps: '2',
 //duration: '60s',
 iterations: 1
};
 
export default function() {
 const CustReg = `{
 "operation":"p2p",
 "sender":"009740000${start_from}",
 "senderType":"M",
 "deviceId":"3",
 "lang":1,
 "msgId":"p2p-${count}${uuidv4()}",
 "tenant":"${tenant}",
 "extraData":[
 {
 "key":"amnt",
 "value":"100"
 },
 {
 "key":"rcvId",
 "value":"009740000${++start_from}"
 },
 {
 "key":"rcvType",
 "value":"M"
 },
 {
 "key":"data",
 "value":"ptptest2"
 }
 ]
 }`;
 count++;
 var params = {
 headers: {
 'Content-Type': 'application/json',
 },
 };
 
 var Coreurl = 'http://192.168.1.249:8080/mpay/soap/process/operation';;
 console.log(Coreurl);
 http.post(Coreurl, CustReg, params);
}
