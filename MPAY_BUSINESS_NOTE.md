**September 14, 2020**

Mobile Banking is a feature provided by the bank to their customer using which customers can do many banking transactions such as requesting loans, statement requests, transferring large amounts of money, and many more. Thus, Mobile Banking is available for only banked customers.
Mobile Payment on the other hand can provide service to both banked as well as non-banked customers. It supports micropayments.

Mobile Payment system as an ecosystem as 3 major components:
1) Payment Service Provider
2) Settlement Bank
3) Clearing System

In general, there are 5 types of workflow in the mPay system:
1) Banked to Banked
A. Onus
B. Different PSPs
2) Banked to Unbanked
3 Cash in Process
4) Cash out Process
5) Unbanked to unbanked

mPay as a product has 2 major components:
1) Customers

Customers are registered using mobile numbers. They interact using the third party application.
2) Corporates

Corporates can further be divided into 3 sub-categories:

Merchant
    Merchant is generally the store. There is no physical cash involved with the merchant. All types of transactions utilize e-money. Types of the transaction are P2M, M2P, M2M
Agents
    Agent work on behalf of the PSP. The PSP will make an agreement with persons who wants to become a agent. The main services of Agents are they will do cash in and cash out for customers on behalf of the PSPs. They will also open an account for the customer by filling in the KYC(Know Your Customer). In contrast to the Merchant where the is no involvement of physical cash, there is the involvement of physical cash in the Agent side.
    Since Agents are working on behalf of PSP, service cash in and cash out will not be informed to clearing house while done with agents. But it will be informed about clearing the house while done with the customer.
Business
    Businesses are allowed features such as Salary disbursement, also they can give loans to the customers.

Limit Profile

Limit Profiles are used to impose limits on different types of Customers. Each customer is applied with one of many limit profiles as per the category it falls under. Each profile of the customer is divided into 5 schemes:
1) Charges scheme

Charges are based on transactions and are also known as fees. Fixed charges are applied in places like a Gas station where the profit margin is less. Whereas the percentage-based charge scheme is applied to markets such as grocery where the profit margin is relatively higher.
2) Limits scheme

Limit schemes are applied to define a limit cap on the transaction of the customer either on a daily, weekly, monthly, or yearly basis. After the customer reaches the limit count for the day he/she will be notified when they try to initiate other transactions. The PSP might have the same limit as defined in the mpclear system but can never exceed.
3) Request types scheme

mPay consists of financial and non-financial message types. Request types scheme is used to limit the customer on the kind of message types they have access to.
4) Commissions scheme

Commissions are provided to Agents based on MOU signed between Agents and PSPs. Each PSP have their own processor or customized commision.
5) Taxes scheme

The tax scheme is to pay tax to the government based on the charge amount paid by the customer to the PSP. There are generally two ways that PSP collects charges from the customer one being the inclusive charge that is combined in the charge amount itself. other being the exclusive charge for which we need to define it in the tax scheme here.



**September 15, 2020**

Each corporate and customer can have multiple wallets under it. The main identifier is the customer id for the customer whereas for corporate it's the registration id. We can create multiple wallets under the same nation ID for a customer if they have multiple mobile numbers.

Customer will have 3 tiers:
1) Customer
2) Customer Mobile
3) Mobile Account

Corporate also has 3 tiers:
1) Corporate
2) Corporate Service
3) Service Account

Account will not be shared between mobiles and services, i.e each account and services will have it's own financial account and it's own profile alongwith balance.
The customer and corporate balance cannot be overdrafted. The system will reject the transaction saying insufficient balance. Similar to the account for customer and corporates, there are also internal acconts for payment service providers.
The PSP can be found as a client type in corporate account but still has it's own accounts. It has internal accounts because any financial system must have internal account when they start the transaction. We have a total of 6 internal accounts:
1) Cash in
2) Cash out
3) Fees
4) Clearing
5) Tax
6) Commission

Once the settle bank is created, the internal accounts will automatically be created. We can find it in Payment Service Providers Management. To see the balance of internal accounts, we can go to corporate service to see it.



**September 16, 2020**

Assignment One:

Create two profile: Profile A and Profile
Profile A has limit for Cash out 1000 QR Daily.
Profile B has limit for Cash out with 500 QR Daily
Profile A has charges of 2 QR with limit up to 1000 QR
Profile B has charges of 2 QR with limit up to 500 QR
Create customer C1 and assign Profile A to it.
Create Customer C2 and assign Profile B to it.
Cash in C1 with 1500 QR.
Cash in C2 with 1000 QR
Cash out C1 with 700 QR
Cash out C2 with 400 QR
Cash out C1 with 800 QR
Cash out C2 with 100 QR
Configure Person to Merchant transaction to be Direct Credit with a minimum amount of 0 and a maximum amount of 1000 QR.

In order to accomplish the above assignment a transaction configuration had to be made for cash in and cash out type transaction and also a limit cap had to be defined because the system was assuming the unlimited balance cap in limit scheme as a numerical zero balance cap.

In addition we also discussed the bug related to unlimited balance cap in the mPay system and general overview of Queries and Integration Logs.



**September 17, 2020**

Create Charge Scheme for Cash Out (Slice up to 100 with 1 QR Charge fixed)
Create Charge Scheme for Cash Out (Slice up to 500 with 2 QR Charge fixed)
Create Charge Scheme for Cash Out (Slice up to 700 with 3 QR Charge Percentage)
Cash out with 90 QR
Cash out with 150 QR
Cash out with 600 QR

The above assignment was accomplished using nested charge scheme i.e using charge slices.

The Queries is the place where we store the integration between ps-mpay and third party. The messages are the ones exchanged between third party application and mPay system. Whereas, the Notification are the those send by the mPay system to the third party applications.

The Integration logs contains 11 different types of logs related to service integrations. Some are
1) Servicve Integration messages: The logs between ps-Mpay and any third party.
2) ps-mpClear Integration Messages: The log between ps-mpay and mpclear
3) Client registration file log: The client registration related logs
4) Bank Integraton message: It is for core banking integration which is not used in current scenario at Qatar

For every message id send by the third party application to the mPay, it will generate a reference id and send it back to the mPay. The message id is always unique to ensure that same transaction is not carried out when the third party API resends the transaction multiple time due to some error in the system. 



**September 20, 2020**

The Security module in the JFW application allows us to define the users and assign custom privileges to them. We first create a business role by defining the generic authorities to it. We assign the generic authorities based on the maker or checker role the business role is to be created for. We then assign the view name that will be granted to this business role. We then create a Group to which the business role is assigned and also incorporating the User.
Lookups

ID Types: used to add id that can be assigned to the customer and corporates
mpClear reasons: It is the mapping of mpClear reasons
Notification types: There are three they are email, SMS, and push notifications
Transaction types: There are only direct debit and direct credit as transaction types in the mpclear system
Transaction Directions: It is of onus, inward, and outward types
Processing statuses: The status of the message be it accepted, failed, pending. rejected or partially accepted
Reasons Configuration: Based on the development. These reasons are sent to third party applications
Channel Types: The communication channels that that mpay supports.
Notification Types: The templated used while sending the notification to the third party application
Limit types: The type that can which is used while defining the limit scheme
Service Integration Reasons: Used for defining the reasons used to communicate with third-party service integration such as card management system in the national switch
jv Types: The types of journal voutchers in the system



**September 21, 2020**

A new PSP has launched in the Market. The PSP decided to work with unbanked customers and corporates with the following Client types information:
1. customer can make up to 3 wallets under the same National ID
2. Merchant can register up to 100 wallets under the same National ID
3. Agent can register up to 500 wallets under the same national ID.

PSP created 3 profiles as the following:
1. Refugees Profile which has no fees on Cash In or Cash out, This profile is not able to do P2P transactions. (Wallet CAP 500 QR).
Daily Limits: P2P: Count = 5 and amount = 200)
Daily Limits: Cash Out: Count = 2 and amount = 500)
2. Regular Profile which has fees on on Cash out with 2 QR for transaction amounts up to 100 QR, and 5 QR up 500 QR, and 3 QR for transaction amount up to 1000 (100% sender , 25% receiver). (Wallet Cap Unlimited)
Daily Limits: P2P: Count = 10 and amount = 800)
Daily Limits: Cash Out: Count = 2 and amount = 100)
3. Children profile which has no fees, this profile will not be able to do any transaction but the Merchant Payment (POS Transaction) and Customer Cash in. (Wallet Cap 50 QR).
Daily Limits: POS: Count = 2 and amount = 50)

1. United nations decided to open 2 Wallets for refugees and to do Cash in for them with 100 QR.
2. One of the united nations customers decided to do Cash out with 50 QR.
3. Someone decided to open a wallets for him and his daughter under the same national id.
4. The father decided to do cash in for his wallet with 1000 QR and for his daughter with 30 QR.
5. The father decided to do cash out for his wallet with 200 QR.
6. The father decided to do cash in for his daughter with 50 QR.

What is needed is to configure the following:
1. POS transaction (Sender customer, Receiver Merchant) Direct Credit.
2. Create Merchant for the daughter's school and assign the Regular Profile to it.
3. Show the transaction which has been made with the status of the transaction (Including Original amount, fees and status).
4. Show the customer balances and calculate how much e-Money the PSP has in the market.
5. Create Multiple Corporate Services for the merchant created in point number 2.
6. Check the Cash out JSON message and try to figure out what is the POS transaction JSON.

The session was all based on the discussion of the task that had been completed for the above assignment.



**September 22, 2020**

Group discussion on Message formats and the important fields in messages sent to and from mpay system. In addition to the accomplishment of all the tasks related to the assignment.



